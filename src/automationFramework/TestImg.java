package automationFramework;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import jasreport.pages.homePage;
import jasreport.pages.tap5_page_imgs;

public class TestImg {

	private List<WebElement> frame;
	private String imgurlBefore[] = new String[5];
	private String imgurlAfter[] = new String[5];

	private int imgsNumber = 5;

	public void startTest(WebDriver driver, WebDriverWait webDriverWait) {
		// go to tap 5
		System.out.println("11");
		homePage.taps_homePage(driver).get(5).click();
		System.out.println("22");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		frame = tap5_page_imgs.frames(driver);
		for (int i = 0; i < imgsNumber; i++) {
			driver.switchTo().frame(frame.get(i));
			imgurlBefore[i] = tap5_page_imgs.find_img(driver).getAttribute("src");
			driver.switchTo().parentFrame();
		}

		/*
		 * System.out.println("start"); Wait<WebDriver> wait = new
		 * FluentWait<WebDriver>(driver) .withTimeout(30, TimeUnit.SECONDS)
		 * .pollingEvery(5, TimeUnit.SECONDS);
		 */
		frame = tap5_page_imgs.frames(driver);
		for (int i = 0; i < imgsNumber; i++) {
			driver.switchTo().frame(frame.get(i));
			imgurlAfter[i] = tap5_page_imgs.find_img(driver).getAttribute("src");
			driver.switchTo().parentFrame();
		}
		
		for (int i = 1; i < imgsNumber; i++) {	
			Assert.assertNotEquals(imgurlBefore[i], imgurlAfter[i]);
		}

	}

}
