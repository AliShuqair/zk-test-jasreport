package automationFramework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.Configration.files.FilesPath;

public class Master {

	WebDriver driver;

	@Test(priority = 1)
	public void startTheBrowserAndGetThePage() {
		// start the fire fox Browser
		System.setProperty("webdriver.gecko.driver", FilesPath.firefoxDriverPath);
		driver = new FirefoxDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// get the page we want to test
		driver.get("http://jasreport.atori.de/JASreport/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = 2)
	public void testTheLoginPage() {
		TestLogInPage logIn = new TestLogInPage("admin", "admin");
		logIn.startTest(driver, new WebDriverWait(driver, 20));
	}

	@Test(priority = 3)
	public void testTheImgInTap5() throws InterruptedException {
		TestImg mm = new TestImg();// use descriptive variable names
		mm.startTest(driver,new WebDriverWait(driver, 20));

	}
	/*
	 * @Test 
	 * public void start4() throws InterruptedException{
	 * TestMainPage main = new TestMainPage(); main.start(driver);
	 * }
	 */

}
