package automationFramework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import jasreport.pages.LoginPage;

public class TestLogInPage {

	private String userName;
	private String password;

	public TestLogInPage(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	@Test
	public void startTest(WebDriver driver, WebDriverWait wait) {

		// check if i get the correct page
		String title = driver.getTitle();
		Assert.assertEquals("JASreport", title);

		// i will enter wrong password to check that it will not pass
		WebElement userFeild = LoginPage.txt_userName(driver);
		userFeild.clear();
		userFeild.sendKeys(userName);

		WebElement passFeild = LoginPage.txt_password(driver);
		passFeild.clear();
		passFeild.sendKeys(password + " *** ");

		WebElement okButton = LoginPage.Button_login(driver);
		okButton.click();

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("z-messagebox-window
		// z-window-highlighted z-window-highlighted-shadow")));

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// close the pop up that show
		WebElement ok = LoginPage.ok_button_popup(driver);
		Assert.assertNotNull(ok);
		ok.click();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// enter the correct user name and password
		userFeild.clear();
		userFeild.sendKeys(userName);

		passFeild.clear();
		passFeild.sendKeys(password);

		okButton.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		/*
		 * 
		 * String expectedUrl = driver.getCurrentUrl(); String actualUrl =
		 * "http://jasreport.atori.de/JASreport/MainWindow.zul";
		 * 
		 * System.out.println(expectedUrl+"\n"+actualUrl);
		 * Assert.assertEquals(actualUrl, expectedUrl ,
		 * "  ** wrong login **  ");
		 * 
		 */
	}

}
