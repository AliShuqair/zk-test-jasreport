
package jasreport.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class homePage {


	public static List<WebElement> taps_homePage(WebDriver driver) {
		return  driver.findElements(By.className("z-tab-hr"));
	}

	public static List<WebElement> nag_homePage(WebDriver driver) {
		return  driver.findElements(By.className("z-tabpanel-accordion-lite-outer"));
	}

	public static WebElement frame_homePage(WebDriver driver) {
		return driver.findElements(By.className("z-iframe")).get(1);
	}
	
	public static WebElement menu_item_homePage(WebDriver driver) {
		return driver.findElement(By.xpath("/html/body/div[1]/header/div[3]/div/div/div/div[1]/div/ul/li[1]/a"));
	}

}
