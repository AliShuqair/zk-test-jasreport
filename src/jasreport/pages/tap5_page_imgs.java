package jasreport.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class tap5_page_imgs {

	private static WebElement element;
	private static List<WebElement> elements;
	
	public static List<WebElement> frames(WebDriver driver) {
		return driver.findElements(By.className("z-iframe"));
	}
	
	public static WebElement find_img(WebDriver driver) {
		return driver.findElement(By.id("__bookmark_1"));
	}
	
	
}
