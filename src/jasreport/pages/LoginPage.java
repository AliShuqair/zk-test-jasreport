
package jasreport.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage {
	
	
	public static WebElement txt_userName(WebDriver driver){
		return driver.findElements(By.className("z-textbox")).get(0);
	}
	
	public static WebElement txt_password(WebDriver driver){
		return  driver.findElements(By.className("z-textbox")).get(1);
	}
	
	public static WebElement Button_login(WebDriver driver){
		return  driver.findElements(By.className("z-button-cm")).get(0);	 
	}
	
	public static WebElement ok_button_popup(WebDriver driver){
		return  driver.findElements(By.className("z-button-cm")).get(2);
	}
	
	

}
